import json, os
import requests

ENDPOINT =  "http://127.0.0.1:8000/api/status/"

image_path = os.path.join(os.getcwd(), "baby-blocks.jpg")

def do_img(method='get', data={}, is_json=True, img_path=None):
	headers = {}
	if is_json:
		headers['content-type'] = 'application/json'
		data = json.dumps(data)
	if img_path is not None:
		with open(image_path, 'rb') as image:
			file_data = {
				'image': image # serializer field
			}
			r = requests.request(method, ENDPOINT, data=data, headers=headers, files=file_data)
	else:
		r = requests.request(method, ENDPOINT, data=data, headers=headers)
	print(r.text)
	print(r.status_code)
	return r


do_img(method='post', data={"user": 1, "content": "hello"}, is_json=False, img_path=image_path)




def do(method='get', data={}, is_json=True):
	headers = {}
	if is_json:
		headers['content-type'] = 'application/json'
		data = json.dumps(data)
	r = requests.request(method, ENDPOINT, data=data, headers=headers)
	print(r.text)
	return r

