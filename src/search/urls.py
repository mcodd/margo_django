from django.conf.urls import url

from .views import (
    SearchMemberListView,

)

urlpatterns = [
    url(r'^$', SearchMemberListView.as_view(), name='query'),
]
