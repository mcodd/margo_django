from django.views.generic import ListView
from django.shortcuts import redirect

from member.models import Member


class SearchMemberListView(ListView):
    template_name = "search/view.html"

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated():
            context = super(SearchMemberListView, self).get_context_data(**kwargs)
            context['query'] = self.request.GET.get('q')
            return context
        else:
            return redirect("login")

    def get_queryset(self, *args, **kwargs):
        request = self.request
        if request.user.is_authenticated():
            user =request.user.get_username()
            query = request.GET.get('q', None)
            if query is not None:
                return Member.objects.search(query).exclude(username=user)
            return Member.objects.all().exclude(username=user)
        else:
            return redirect("login")
