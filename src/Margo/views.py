from django.utils import timezone
from datetime import timedelta
from django.shortcuts import render, redirect
from .forms import ContactForm

from member.models import Member
from billing.models import BillingProfile
from paymargo.models import Paymargo


def home_page(request):
    user = request.user
    if user.is_authenticated():
        un = user.get_username()
        member = Member.objects.get(username=un)
        billing_profile = BillingProfile.objects.get(user=member)
        paym_obj = Paymargo.objects.get(billing_profile=billing_profile)
        check_unlimited = timezone.now()
        month_sub = timedelta(days=30)
        if paym_obj.updated + month_sub < check_unlimited:
            paym_obj.unlimited = False

        context = {
            'name': member.name,
            'balance': billing_profile.balance,
            'image': member.image
        }
        return render(request, 'home_page.html', context)
    else:
        return redirect("login")


def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        'title': 'Contactanos',
        'content': 'Dinos como te podemos ayudar!',
        'form': contact_form,
    }
    if contact_form.is_valid():
        print(contact_form.cleaned_data)
    return render(request, 'contact/view.html', context)
