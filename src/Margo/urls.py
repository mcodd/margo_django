"""Margo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.conf import settings
from django.conf.urls.static import static

from accounts.views import (
    login_page,
    register_page,
    register_page_2
)
from .views import home_page, contact_page


urlpatterns = [
    url(r'^home/$', home_page, name='home'),
    url(r'^contact/$', contact_page, name='contact'),
    url(r'^$', login_page, name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^register/$', register_page,name='register'),
    url(r'^register-2/$', register_page_2,name='register-2'),
    url(r'^bank/', include('banks.urls', namespace='banks')),
    url(r'^cart/', include('carts.urls', namespace='cart')),
    url(r'^paym/', include('paymargo.urls', namespace='paymargo')),
    url(r'^members/', include('member.urls', namespace='members')),
    url(r'^search/', include('search.urls', namespace='search')),
    url(r'^api/status/', include('status.api.urls', namespace='status')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
