from django import forms


class ContactForm(forms.Form):
    fullname = forms.CharField(
        label='Nombre Completo',
        widget=forms.TextInput(
            attrs=
            {"class": "form-control"}
        )
    )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control",
                "placeholder": "Correo electronico"
            }
        )
    )

    content = forms.CharField(
        label='Mensaje',
        widget=forms.Textarea(
            attrs={"class": "form-control"}
        )
    )

    def clean_email(self):
        email = self.cleaned_data.get('email')
        return email
