from django.conf.urls import url

from .views import (
    StatusAPIView,
    # StatusDetailAPIView,
    # StatusUpdateAPIView,
    # StatusDeleteAPIView
)


urlpatterns = [
    url(r'^$', StatusAPIView.as_view()),
    # url(r'^(?P<pk>\d+)/$', StatusDetailAPIView.as_view()),  # can change pk for id or username
    # url(r'^(?P<pk>\d+)/update/$', StatusUpdateAPIView.as_view()),
    # url(r'^(?P<pk>\d+)/delete/$', StatusDeleteAPIView.as_view()),
]
