from django.shortcuts import render, redirect

from .models import Cart
from .forms import SendForm
from billing.models import BillingProfile
from orders.models import Order
from member.models import Member
from paymargo.models import Paymargo


def cart_home(request):
    if request.user.is_authenticated():
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        user = request.user.get_username()
        member_un = Member.objects.get(username=user)
        billing_profile = BillingProfile.objects.get(user=member_un)
        balance = billing_profile.balance
        form = SendForm(request.POST or None, balance=balance)
        content = {
            'cart': cart_obj,
            'balance': balance,
            'form': form
        }
        if form.is_valid():
            amount = form.cleaned_data.get('amount')
            cart_obj.amount = amount

            # view checks for overdraft
            paym_obj = Paymargo.objects.get(billing_profile=billing_profile)
            if paym_obj.unlimited:
                paym_cost, paym_go = 0, True
            else:
                user_remainder = paym_obj.remaining
                paym_cost, paym_go = pay_margo_update(user_remainder, amount)

            cart_obj.margo_cost = paym_cost
            cart_obj.save()
            if not paym_go:
                return redirect("paymargo:check")
            return redirect("cart:checkout")
        return render(request, "carts/home.html", content)
    else:
        return redirect("login")


def pay_margo_update(user_remainder, amount):
    total = user_remainder + amount
    x = total//500
    to_pay = x*3
    if x >= 1:
        return to_pay, False
    else:
        return to_pay, True


def cart_update(request):
    member_id = request.POST.get('member_id')
    member_obj = Member.objects.get(id=member_id)
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    cart_obj.member = member_obj
    cart_obj.save()
    return redirect("cart:home")


def remove_cart(request):
    member_id = request.POST.get('member_id')
    member_obj = Member.objects.get(id=member_id)
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    if member_obj.id == cart_obj.member.id:
        cart_obj.member = None
        cart_obj.save()
        return redirect("search:query")
    return redirect("cart:home")


def checkout_home(request):
    if request.user.is_authenticated():
        user = request.user.get_username()
        member_obj = Member.objects.get(username=user)
        cart_obj, cart_created = Cart.objects.new_or_get(request)
        order_obj = None

        if cart_created:
            return redirect("cart:home")

        billing_profile = BillingProfile.objects.get(user=member_obj)
        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile=billing_profile, cart_obj=cart_obj)

        if request.method == "POST":
            is_done = order_obj.check_done()
            if is_done:
                order_obj.mark_done()
                del request.session['cart_id']
                return redirect("cart:success")

        context = {
            "object": order_obj,
            "billing_profile": billing_profile
        }
        return render(request, "carts/checkout.html", context)
    else:
        return redirect("login")


def checkout_done_view(request):
    return render(request, "carts/checkout-done.html", {})
