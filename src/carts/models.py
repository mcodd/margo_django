from django.db import models
from django.conf import settings

from member.models import Member

User = settings.AUTH_USER_MODEL


class CartManager(models.Manager):
    def new_or_get(self, request):
        cart_id = request.session.get("cart_id", None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.count() == 1:
            created = False
            obj = qs.first()
            if request.user.is_authenticated() and obj.user is None:
                obj.user = request.user
                obj.save()
        else:
            obj = self.new_cart(user=request.user)
            created = True
            request.session['cart_id'] = obj.id
        return obj, created

    def new_cart(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated():
                user_obj = user
        return self.model.objects.create(user=user_obj)


class Cart(models.Model):
    user            = models.ForeignKey(User, null=True, blank=True)
    member          = models.ForeignKey(Member, null=True, blank=True)
    amount          = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    margo_cost      = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    timestamp       = models.DateTimeField(auto_now_add=True)

    objects = CartManager()

    def __str__(self):
        return str(self.id)
