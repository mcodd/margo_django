from django import forms


class SendForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.balance = kwargs.pop('balance')
        super(SendForm, self).__init__(*args, **kwargs)

    amount = forms.DecimalField(decimal_places=2, max_digits=100)

    def clean_amount(self):
        amount = self.cleaned_data.get('amount')
        if amount < 3:
            raise forms.ValidationError('Monto minimo de envio: 3.00 dolares')
        elif amount > self.balance:
            raise forms.ValidationError('Monto es mayor a balance actual')
        else:
            return amount
