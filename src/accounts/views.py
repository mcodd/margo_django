from django.contrib.auth import authenticate, login, get_user_model
from django.shortcuts import render, redirect
from .forms import LoginForm, RegisterForm, RegisterTwoForm

from member.models import Member


User = get_user_model()


def login_page(request):
    form = LoginForm(request.POST or None)
    context = {
        'form': form,
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            try:
                del request.session['temp_data']
            except:
                pass
            return redirect('home')
        else:
            print("Error")
    return render(request, 'accounts/login.html', context)


def register_page(request):
    form = RegisterForm(request.POST or None)
    context = {
        'form': form,
    }
    if form.is_valid():
        fullname = form.cleaned_data.get('full_name')
        identity = form.cleaned_data.get('identity')
        email = form.cleaned_data.get('email')
        phone_num = form.cleaned_data.get('phone_num')
        request.session['temp_data'] = [fullname, identity, email, phone_num]
        return redirect("register-2")

    return render(request, 'accounts/register.html', context)


def register_page_2(request):
    form = RegisterTwoForm(request.POST or None)
    data = request.session.get('temp_data')
    if data is None:
        return redirect("register")

    fullname    = data[0]
    identity    = data[1]
    email       = data[2]
    phone_num   = data[3]

    context = {
        'form': form,
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        User.objects.create_user(username, email, password)
        Member.objects.create(name=fullname, identity=identity, username=username, phone=phone_num)
        return redirect('login')

    return render(request, 'accounts/register-2.html', context)
