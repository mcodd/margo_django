import re, string
from django import forms
from django.contrib.auth import get_user_model

from member.models import Member

User = get_user_model()


def ispunct(s):
    for l in s:
        if l in string.punctuation:
            return True
    return False


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean_username(self):
        un = self.cleaned_data.get('username')
        qs = User.objects.filter(username=un)
        if qs.count() == 0:
            raise forms.ValidationError("Usuario no existe")
        return un


class RegisterForm(forms.Form):
    full_name = forms.CharField(label='Nombre Completo', max_length=50)
    identity = forms.CharField(label='Cedula', max_length=13)
    email = forms.EmailField(label='Email')
    phone_num = forms.CharField(label='Telefono Movil', max_length=9)

    def clean_full_name(self):
        fn = self.cleaned_data.get('full_name')
        return fn.title()

    def clean_identity(self):
        ident = self.cleaned_data.get('identity')
        if len(ident) != 10:
            raise forms.ValidationError("Cédula incompleta.")
        else:
            qs = Member.objects.filter(identity=ident)
            if qs.exists():
                raise forms.ValidationError("Cédula ya en uso.")
        return ident

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("Email ya en uso")
        return email

    def clean_phone_num(self):
        pn = self.cleaned_data.get('phone_num')
        if len(pn) != 9:
            raise forms.ValidationError("Numero movil incompleto")
        return pn


class RegisterTwoForm(forms.Form):
    username = forms.CharField(label='Nombre de usuario')
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)

    def clean_username(self):
        un = self.cleaned_data.get('username')
        username = un.lower()
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError("Nombre de usuario ya en uso")
        while True:
            if re.search("\s", username):
                flag = False
                break
            elif ispunct(username):
                flag = ispunct(username)
                break
            else:
                flag = True
                break

        if not flag:
            raise forms.ValidationError('Nombre de usuario solo puede tener letras y numeros')
        return username

    def clean(self):
        data = self.cleaned_data
        password = self.cleaned_data.get('password')
        message = 'Contraseña debe contener minimo 8 caracteres entre letras mayusculas, minisculas, y numeros'
        while True:
            if len(password) < 8:
                flag = False
                break
            elif not re.search("[a-z]", password):
                flag = False
                break
            elif not re.search("[A-Z]", password):
                flag = False
                break
            elif not re.search("[0-9]", password):
                flag = False
                break
            elif re.search("\s", password):
                flag = False
                break
            else:
                flag = True
                break

        if not flag:
            raise forms.ValidationError(message)
        password2 = self.cleaned_data.get('password2')
        if password2 != password:
            raise forms.ValidationError('Contraseñas deben coincidir')
        return data
