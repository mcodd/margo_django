from math import fsum
from django.utils import timezone
from django.db import models
from django.db.models.signals import pre_save, post_save

from billing.models import BillingProfile
from paymargo.models import Paymargo
from carts.models import Cart
from Margo.utils import unique_order_id_generator


ORDER_STATUS_CHOICES =(
    ('created', 'Created'),
    ('paid', 'Paid'),
    ('refunded', 'Refunded'),

)


class OrderManager(models.Manager):
    def new_or_get(self, billing_profile, cart_obj):
        created = False
        qs = self.get_queryset().filter(
                billing_profile=billing_profile,
                cart=cart_obj,
                active=True,
                status='created')

        if qs.count() == 1:
            obj = qs.first()
        else:
            obj = self.model.objects.create(billing_profile=billing_profile, cart=cart_obj)
            created = True
        return obj, created


class Order(models.Model):
    order_id            = models.CharField(max_length=120, null=True, blank=True)
    billing_profile     = models.ForeignKey(BillingProfile, blank=True, null=True)
    cart                = models.ForeignKey(Cart)
    status              = models.CharField(max_length=120, default='created', choices=ORDER_STATUS_CHOICES)
    order_total         = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    active              = models.BooleanField(default=True)

    objects = OrderManager()

    def __str__(self):
        return self.order_id

    def update_total(self):
        total = fsum([self.cart.amount, self.cart.margo_cost])
        new_total = format(total, '.2f')
        self.order_total = new_total
        self.save()
        return new_total

    def check_done(self):
        billling_profile = self.billing_profile
        balance = billling_profile.balance
        total = self.order_total
        if 0 < total <= balance:
            return True
        return False

    def mark_done(self):
        if self.check_done():
            amount = self.cart.amount
            billing_profile = self.billing_profile
            member = self.cart.member
            margo_cost = self.cart.margo_cost

            member_profile = BillingProfile.objects.get(user=member)
            paym = Paymargo.objects.get(billing_profile=billing_profile)

            if margo_cost > 0 and margo_cost%3 == 0:
                paym.member_type = 'one-time-paid'

            if margo_cost == 20:
                paym.unlimited = True
                paym.member_type = 'month-paid'
                paym.updated = timezone.now()

            new_balance = billing_profile.balance - amount
            member_new_balance = member_profile.balance + amount

            billing_profile.balance = new_balance
            member_profile.balance = member_new_balance

            paym.paid_margo += self.cart.margo_cost
            paym.remaining += amount

            billing_profile.save()
            member_profile.save()
            paym.adjust_remainder()

            self.status = "paid"
            self.save()
        return self.status


def pre_save_create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)
    qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
    if qs.exists():
        qs.update(active=False)


pre_save.connect(pre_save_create_order_id, sender=Order)


def post_save_cart_total(sender, instance, created, *args, **kwargs):
    if not created:
        cart_obj = instance
        cart_id = cart_obj.id
        qs = Order.objects.filter(cart__id=cart_id)
        if qs.count() == 1:
            order_obj = qs.first()
            order_obj.update_total()


post_save.connect(post_save_cart_total, sender=Cart)



def post_save_order(sender, instance, created, *args, **kwargs):
    if created:
        instance.update_total()


post_save.connect(post_save_order, sender=Order)
