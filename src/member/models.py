import random
import os
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.conf import settings


User = settings.AUTH_USER_MODEL


def get_filename(filename):
    base_name = os.path.basename(filename)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 100000)
    name, ext = get_filename(filename)
    final_filename = f'{new_filename}{ext}'
    return f'member/{new_filename}/{final_filename}'


class MemberQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)


    def search(self, query):
        lookups = (
                Q(name__icontains=query) |
                Q(username__icontains=query) |
                Q(phone__icontains=query)
                )
        qs = self.filter(lookups).distinct()
        return qs


class MemberManager(models.Manager):
    def get_queryset(self):
        return MemberQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().active().search(query)


class Member(models.Model):
    name            = models.CharField(max_length=120)
    identity        = models.CharField(max_length=13)
    username        = models.CharField(max_length=30)
    phone           = models.CharField(max_length=10, blank=True, null=True)
    active          = models.BooleanField(default=True)
    rating          = models.DecimalField(default=5.0, decimal_places=1, max_digits=10)
    image           = models.ImageField(default='default_img.jpg', upload_to=upload_image_path, null=True, blank=True)

    objects = MemberManager()  # no override, just extends

    def get_absolute_url(self):
        return reverse("members:detail", kwargs={'username': self.username})

    def __str__(self):
        return self.name.title()
