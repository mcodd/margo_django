# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2019-01-11 21:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0012_auto_20190111_2115'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='member',
            name='slug',
        ),
    ]
