# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2019-01-13 17:51
from __future__ import unicode_literals

from django.db import migrations, models
import member.models


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0013_remove_member_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=member.models.upload_image_path),
        ),
    ]
