from django.shortcuts import redirect
from django.http import Http404
from django.views.generic import ListView, DetailView

from carts.models import Cart

from .models import Member


class MemberListView(ListView):
    template_name = "member/list.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        if request.user.is_authenticated():
            return Member.objects.all()
        else:
            return redirect("login")


class MemberDetailView(DetailView):
    queryset = Member.objects.all()
    template_name = "member/detail.html"

    def get_context_data(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            context = super(MemberDetailView, self).get_context_data(*args, **kwargs)
            cart_obj, new_obj = Cart.objects.new_or_get(self.request)
            context['cart'] = cart_obj
            return context
        else:
            return redirect("login")

    def get_object(self, *args, **kwargs):
        request = self.request
        if request.user.is_authenticated():
            username = self.kwargs.get('username')
            try:
                instance = Member.objects.get(username=username, active=True)
            except Member.DoesNotExist:
                raise Http404("Not Found..")
            except Member.MultipleObjectsReturned:
                qs = Member.objects.filter(username=username, active=True)
                instance = qs.first()
            except:
                raise Http404("MMMmmm")
            return instance
        else:
            return redirect("login")

