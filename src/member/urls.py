from django.conf.urls import url

from .views import (
    MemberListView,
    MemberDetailView,
    # MemberDetailSlugView
)

urlpatterns = [
    url(r'^$', MemberListView.as_view(), name='list'),
    url(r'^(?P<username>[\w-]+)/$', MemberDetailView.as_view(), name='detail'),
    # url(r'^(?P<slug>[\w-]+)/$', MemberDetailSlugView.as_view(), name='detail'),
]
