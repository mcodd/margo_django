from django.db import models
from django.db.models.signals import post_save

from member.models import Member


class BillingProfile(models.Model):
    user            = models.OneToOneField(Member, null=True, blank=True)
    balance         = models.DecimalField(default=0.00, decimal_places=2, max_digits=1000)
    active          = models.BooleanField(default=True)
    updated         = models.DateTimeField(auto_now=True)
    timestamp       = models.DateTimeField(auto_now_add=True)
    # customer_id

    def __str__(self):
        return self.user.username


def user_created_receiver(sender, instance, created, *args, **kwargs):
    """
    When a User is created a billing profile is created with it
    """
    if created:
        BillingProfile.objects.get_or_create(user=instance)


post_save.connect(user_created_receiver, sender=Member)
