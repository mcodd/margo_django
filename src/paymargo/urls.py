from django.conf.urls import url


from .views import (
    checkout_paymargo_view,
    margo_payment_type
)

urlpatterns = [
    url(r'^checkout/$', checkout_paymargo_view, name='check'),
    url(r'^pay-option/$', margo_payment_type, name='option'),
]
