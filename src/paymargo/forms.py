from django import forms


class PaymargoForm(forms.Form):
    one_time_paid   = forms.CharField()
    month_paid      = forms.CharField()
