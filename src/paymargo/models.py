from django.db import models
from django.db.models.signals import post_save

from billing.models import BillingProfile


STATUS_CHOICES =(
    ('free', 'Free'),
    ('one-time-paid', 'One-Time-Paid'),
    ('month-paid', 'Month-Paid'),
    ('refunded', 'Refunded'),
)


class Paymargo(models.Model):
    billing_profile         = models.ForeignKey(BillingProfile)
    remaining               = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    paid_margo              = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    member_type             = models.CharField(max_length=120, default='free', choices=STATUS_CHOICES)
    unlimited               = models.BooleanField(default=False)
    updated                 = models.DateTimeField(auto_now=True)
    timestamp               = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.billing_profile.user.username

    def adjust_remainder(self):
        remainder = self.remaining
        total = float(remainder)//500
        if total >= 1:
            deduct = total * 500
            new_remaining = float(remainder) - deduct
            self.remaining = format(new_remaining, '.2f')
            self.save()
        return self.remaining


def user_created_receiver(sender, instance, created, *args, **kwargs):
    if created:
        Paymargo.objects.get_or_create(billing_profile=instance)


post_save.connect(user_created_receiver, sender=BillingProfile)


def remaining_adjuster_receiver(sender, instance, created,  *args, **kwrags):
    instance.adjust_remainder()


post_save.connect(remaining_adjuster_receiver, sender=Paymargo)
