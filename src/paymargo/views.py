from django.shortcuts import render, redirect

from carts.models import Cart


UNLIMITED = 20.00


def checkout_paymargo_view(request):

    cart_obj, cart_created = Cart.objects.new_or_get(request)

    context = {
        'cart_obj': cart_obj,
        'unlimited': format(UNLIMITED, '.2f')
    }

    return render(request, 'paymargo/paym.html', context)


def margo_payment_type(request):

    pay_option = request.POST.get('pay_option')
    margo_cost = float(pay_option)
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    cart_obj.margo_cost = margo_cost
    cart_obj.save()

    return redirect("cart:checkout")
