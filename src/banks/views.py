from django.shortcuts import render, redirect

from .forms import AddBankAccountForm
from . models import BankAccount
from member.models import Member


def bank_home(request):
    user= request.user
    if user.is_authenticated():
        member = Member.objects.get(username=user.get_username())
        bank_obj = BankAccount.objects.filter(user=member)
        context = {
            'bank_obj': bank_obj
        }
        return render(request, 'banks/home.html', context)
    else:
        return redirect('login')


def add_bank(request):
    user = request.user
    if user.is_authenticated():
        member = Member.objects.get(username=user.get_username())
        bank_obj = BankAccount.objects.filter(user=member)

        form = AddBankAccountForm(request.POST or None)

        context = {
            'bank_obj': bank_obj,
            'form': form
        }
        if form.is_valid():
            bn = form.cleaned_data.get('bank_name')
            nn = form.cleaned_data.get('nickname')
            at = form.cleaned_data.get('account_type')
            an = form.cleaned_data.get('account_number')
            BankAccount.objects.create(user=member, nickname=nn, bank_name=bn, account_type=at, account_number=an)
            return redirect('banks:home')
        return render(request, 'banks/add-account.html', context)
    else:
        return redirect("login")
