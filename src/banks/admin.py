from django.contrib import admin

from .models import BankAccount


class BankAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'account_number']

    class Meta:
        model = BankAccount


admin.site.register(BankAccount, BankAdmin)
