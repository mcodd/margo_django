from django.conf.urls import url

from .views import (
    bank_home,
    add_bank,
)

urlpatterns = [
    url(r'^$', bank_home, name='home'),
    # url(r'^checkout/success/$', checkout_done_view, name='success'),
    url(r'^add/$', add_bank, name='add'),
    # url(r'^update/$', cart_update, name='update'),
    # url(r'^remove/$', remove_cart, name='remove'),

]
