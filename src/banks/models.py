from django.db import models

from member.models import Member


ACCOUNT_TYPES = (
    ('checking', 'Checking'),
    ('savings', 'Savings'),
)

BANK_NAMES = (
    ('Pacifico', 'Pacifico'),
    ('Produbanco', 'Produbanco'),
    ('Bolivariano', 'Bolivariano'),
    ('Guayaquil', 'Guayaquil'),
    ('Internacional', 'Internacional'),
    ('Pichincha', 'Pichincha'),
)


class BankAccount(models.Model):
    user            = models.ForeignKey(Member, null=True)
    nickname        = models.CharField(max_length=120, blank=True, null=True)
    bank_name       = models.CharField(max_length=120, choices=BANK_NAMES)
    account_type    = models.CharField(max_length=120, choices=ACCOUNT_TYPES)
    account_number  = models.CharField(max_length=15)
    valid           = models.BooleanField(default=False)

    def __str__(self):
        if self.nickname:
            return self.nickname.title()
        return self.account_number

    def get_account_number(self):
        acc_num = str(self.account_number)
        return "***" + acc_num[-4:]
