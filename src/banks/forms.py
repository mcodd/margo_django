from django import forms

from .models import BankAccount

ACCOUNT_TYPES = (
    ('checking', 'Checking'),
    ('savings', 'Savings'),
)

BANK_NAMES = (
    ('Pacifico', 'Pacifico'),
    ('Produbanco', 'Produbanco'),
    ('Bolivariano', 'Bolivariano'),
    ('Guayaquil', 'Guayaquil'),
    ('Internacional', 'Internacional'),
    ('Pichincha', 'Pichincha'),
)


class AddBankAccountForm(forms.Form):
    bank_name = forms.ChoiceField(
        label='Nombre del banco',
        widget=forms.Select,
        choices= BANK_NAMES,
        )

    nickname = forms.CharField(
        label="Apodo de la cuenta (opcional)",
        max_length=50,
        required=False
    )

    account_type = forms.ChoiceField(
        label='Tipo de cuenta',
        widget= forms.Select,
        choices=ACCOUNT_TYPES
        )

    account_number = forms.CharField(
        label='Numero de cuenta',
        max_length=50,
        )

    def clean_account_number(self):
        an = self.cleaned_data.get('account_number')
        bn = self.cleaned_data.get('bank_name')
        qs = BankAccount.objects.filter(bank_name=bn, account_number=an)
        if qs.exists():
            raise forms.ValidationError("Cuenta ya en uso")
        return an

    def clean_bank_name(self):
        bn = self.cleaned_data.get('bank_name')
        return bn

    def clean_account_type(self):
        at = self.cleaned_data.get('account_type')
        return at

    def clean_nickname(self):
        nn = self.cleaned_data.get('nickname')
        return nn.title()

